#!/bin/bash
# ============================================
# Script that modifies the cts and xml results
# to do more easier to continue sessions.
#
# author: Paulo Honório - 14/12/2015
# ============================================
#http://stackoverflow.com/questions/893585/how-to-parse-xml-in-bash

read_dom () {
    local IFS=\/\>
    read -d \< ENTITY CONTENT
    local ret=$?
    TAG_NAME=${ENTITY%% *}
    ATTRIBUTES=${ENTITY#*}
    return $ret
}

parse_dom () {
    #Parse fail counts to notExecuuted in Summary
    if [[ $TAG_NAME = "Summary" ]] ; then
        eval local $ATTRIBUTES
        echo "<Summary failed=\"$failed\" notExecuted=\"$notExecuted\" timeout=\"$timeout\" pass=\"$pass\" />"
        sed "s/Summary failed=\"$failed\" notExecuted=\"$notExecuted\"/Summary failed=\"0\" notExecuted=\"$(($notExecuted+$failed))\"/" "$@" > "$@~"
        break
    fi
}

ler_xml(){
    while read_dom; do
        parse_dom $@
    done < $@
    #Parse fail results to notExecuted in tests
    sed "s/result=\"fail\"/result=\"notExecuted\"/" "$@~" > "$@"
}

main()
{
    if [ "$@." != "." ];
    then
		ler_xml $@
        cat "$@" | grep \<Summary
        cat "$@" | grep result=\"fail\"
        rm -rf "$@~"
    else
        echo "Informe o xml com result do CTS ou GTS"
    fi
}

main $@
